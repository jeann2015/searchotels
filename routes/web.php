<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'RootController@index')->name('/');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('verifyemail/{token}', 'Auth\RegisterController@verify');


/**Users**/
Route::get('/users', 'UserController@index')->name('users');
Route::get('/users/add', 'UserController@add')->name('users/add');
Route::post('/users/new', 'UserController@news')->name('users/new');
Route::get('/users/edit/{id}', 'UserController@edit');
Route::post('/users/update', 'UserController@update');
Route::get('/users/delete/{id}', 'UserController@delete');
Route::post('/users/destroy', 'UserController@destroy');
/**Users**/

/**Audit**/
Route::get('/audit', 'AuditController@index')->name('audit');
/**Audit**/

/**Password**/
Route::post('/changes', 'UserController@changes')->name('changes');
/**Password**/

/**Profile**/
Route::get('/profile', 'UserController@profiles')->name('profile');

Route::get('/settings', 'UserController@settings')->name('settings');
Route::post('/update', 'UserController@update_profile')->name('update');
/**Profile**/

/**Module**/
Route::get('/modules', 'ModuleController@index')->name('modules');
/**Module**/

/**Searching**/
Route::post('search/searching', 'BuscadorController@search')->name('search/searching');
Route::post('search/searching/welcome', 'RootController@search')->name('search/searching/welcome');
/**Searching**/

/**Hotels**/
Route::get('search/hotels/{hotels_id}/{search_code}', 'HotelsController@search')->name('search/hotels');
/**Hotels**/