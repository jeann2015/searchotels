<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','email_token','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @param $user_id
     * @param $group_id
     * @return int
     */
    public function user_register_entry_modules($user_id, $group_id)
    {

        $groupsmodules = new GroupModule;
        $modules = Module::all();

        $groupusers = new GroupUser;
        $groupusers->users_id = $user_id;
        $groupusers->groups_id = $group_id;
        $groupusers->save();

        foreach ($modules as $module) {
            $gruposmodules_all = $groupsmodules->grupos_modulos($group_id, $module->id);


            foreach ($gruposmodules_all as $gruposmodule) {

                $view_value = $gruposmodule->views;
                if ($view_value=="") {
                    $view_value="0";
                }

                $save_value = $gruposmodule->inserts;
                if ($save_value=="") {
                    $save_value="0";
                }

                $modify_value = $gruposmodule->modifys;
                if ($modify_value=="") {
                    $modify_value="0";
                }

                $delete_value = $gruposmodule->deletes;
                if ($delete_value=="") {
                    $delete_value="0";
                }

                if ($view_value<>"" || $save_value<>"" || $modify_value<>"" || $delete_value<>"") {
                    $entry = new Entry;
                    $entry->users_id=$user_id;
                    $entry->modules_id=$module->id;
                    $entry->views=$view_value;
                    $entry->inserts=$save_value;
                    $entry->modifys=$modify_value;
                    $entry->deletes=$delete_value;
                    $entry->status=1;
                    $entry->save();
                }
            }
        }

        return 0;
    }

    /**
     * @param $users_id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function get_users_profiles($users_id){

        $users = self::join('user_profiles','user_profiles.users_id','=','users.id')
            ->select('users.id','users.name','users.email','users.status',
                'user_profiles.address','user_profiles.phone','user_profiles.airport',
                'user_profiles.city',
                'user_profiles.passport',
                'user_profiles.images')
            ->where('users.id',$users_id)
            ->get();

        return $users;
    }
}
