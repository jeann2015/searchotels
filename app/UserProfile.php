<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table="user_profiles";

    protected $fillable = [
        'users_id','address','phone','airport','city','passport','images'
    ];
}
