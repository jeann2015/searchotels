<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupModule extends Model
{
    protected $table="group_modules";

    /**
     * @param $grupo
     * @param $module
     * @return mixed
     */
    public function grupos_modulos($grupo, $module)
    {
        $gruposmodule = self::where('groups_id', '=', $grupo)
            ->where('modules_id', '=', $module)
            ->get();

        return $gruposmodule;
    }
}
