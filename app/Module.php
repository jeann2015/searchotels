<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table="modules";

    protected $fillable = [
        'description','order','id_father','url','messages','status','visible','clase'
    ];

    /**
     * @param $modules_id
     */
    public function add_new_user_modules($modules_id)
    {
        $users = User::all();
        foreach ($users as $user) {
            $entrys = new Entry;
            $entrys->users_id=$user->id;
            $entrys->modules_id=$modules_id;
            $entrys->views=0;
            $entrys->inserts=0;
            $entrys->modifys=0;
            $entrys->deletes=0;
            $entrys->status=1;
            $entrys->save();
        }
    }

    /**
     * @param $iduser
     * @param $url
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function accesos($iduser,$url)
    {
        $urls = explode("/",$url);

        $select = self::select('entries.views',
                'entries.inserts',
                'entries.modifys',
                'entries.deletes')
            ->join('entries', function ($join) {
                $join->on('entries.modules_id', '=', 'modules.id');
            });

        $select->where('entries.users_id', '=', $iduser)
            ->where('modules.url', '=', $urls[0])
            ->where('modules.status', '=', '1');

        $data = $select->orderBy('modules.id', 'asc')->get();

        return $data;
    }

    /**
     * @param $iduser
     * @return mixed
     */
    public function get_modules_principal_user($iduser)
    {
        $module_principal = \DB::table('modules')
            ->select('modules.id',
                'modules.description',
                'modules.order',
                'modules.id_father',
                'modules.url',
                'modules.messages',
                'modules.status',
                'modules.visible',
                'entries.views',
                'entries.inserts',
                'entries.modifys',
                'entries.deletes')
            ->join('entries', function ($join) use ($iduser) {
                $join->on('entries.modules_id', '=', 'modules.id')->where('entries.users_id', '=', $iduser)
                    ->where('modules.url', '=', '#');
            })
            ->where('entries.users_id', '=', $iduser)
            ->where('modules.status', '=', '1')
            ->where('modules.visible', '=', '1')
            ->orderBy('modules.order')->orderBy('modules.id')->get();

        return  $module_principal;
    }

    /**
     * @param $iduser
     * @return mixed
     */
    public function get_modules_menu_user($iduser)
    {
        $module_menu = \DB::table('modules')
            ->select('modules.id',
                'modules.description',
                'modules.order',
                'modules.id_father',
                'modules.url',
                'modules.messages',
                'modules.status',
                'modules.visible',
                'entries.views',
                'entries.inserts',
                'entries.modifys',
                'entries.deletes')
            ->join('entries', function ($join) use ($iduser) {
                $join->on('entries.modules_id', '=', 'modules.id')->where('entries.users_id', '=', $iduser)
                    ->where('modules.url', '<>', '#');
            })
            ->where('entries.users_id', '=', $iduser)
            ->where('modules.status', '=', '1')
            ->where('modules.visible', '=', '1')
            ->orderBy('modules.order')->get();

        return $module_menu;
    }
}
