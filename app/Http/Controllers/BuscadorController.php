<?php

namespace App\Http\Controllers;

use App\Buscador;
use App\Module;
use Illuminate\Http\Request;

class BuscadorController extends Controller
{
    /**
     * BuscadorController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search(Request $request)
    {

        $this->validate($request, [
            'destination' => 'required',
            'checkin' => 'required:date',
            'checkout' => 'required:date',
            'guest' => 'required:numeric'
        ]);

        return redirect()->route('home', ['destination'=>$request->destination,'checkin'=>$request->checkin,'checkout'=>$request->checkout,'guest'=>$request->guest]);
    }


}
