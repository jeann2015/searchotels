<?php

namespace App\Http\Controllers;

use App\Buscador;
use App\Module;
use Illuminate\Http\Request;

class RootController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $b = new Buscador;
        $results=array();
        $datos=array();
        $countries = $b->get_countries();

        if (isset($request->destination) && isset($request->checkin) && isset($request->checkout) && isset($request->guest)) {
            $results = $b->get_hotels_sales($request->destination, $request->checkin, $request->checkout, $request->guest);

            $datos['destination']=$request->destination;
            $datos['checkin']=$request->checkin;
            $datos['checkout']=$request->checkout;
            $datos['guest']=$request->guest;
        }else{
        $results['0']=0;
        }

        return view('welcome', compact('countries', 'results', 'datos'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'destination' => 'required',
            'checkin' => 'required:date',
            'checkout' => 'required:date',
            'guest' => 'required:numeric'
        ]);

        return redirect()->route('/', ['destination'=>$request->destination,'checkin'=>$request->checkin,'checkout'=>$request->checkout,'guest'=>$request->guest]);
    }
}
