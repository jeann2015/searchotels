<?php

namespace App\Http\Controllers;

use App\Buscador;
use App\Module;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $b = new Buscador;
        $results=array();
        $datos=array();
        $countries = $b->get_countries();

        $users_id_current = \Auth::id();
        $module = new Module;
        $module_principals = $module->get_modules_principal_user($users_id_current);
        $module_menus = $module->get_modules_menu_user($users_id_current);


        if (isset($request->destination) && isset($request->checkin) && isset($request->checkout) && isset($request->guest)) {
            $results = $b->get_hotels_sales($request->destination, $request->checkin, $request->checkout, $request->guest);

            $datos['destination']=$request->destination;
            $datos['checkin']=$request->checkin;
            $datos['checkout']=$request->checkout;
            $datos['guest']=$request->guest;
        }else{
            $results['0']=0;
        }

        return view('home', compact('module_menus','countries', 'results', 'datos','module_principals'));
    }


}
