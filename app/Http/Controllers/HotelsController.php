<?php

namespace App\Http\Controllers;

use App\Buscador;
use App\Module;
use Illuminate\Http\Request;

class HotelsController extends Controller
{
    /**
     * HotelsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $b = new Buscador;
        $users_id_current = \Auth::id();
        $module = new Module;
        $module_principals = $module->get_modules_principal_user($users_id_current);
        $module_menus = $module->get_modules_menu_user($users_id_current);

        $hotels_result = $b->get_hotels($request->hotels_id,$request->search_code);

//        dd($hotels_result);

        return view('hotel.index', compact( 'module_menus','module_principals','hotels_result'));
    }
}
