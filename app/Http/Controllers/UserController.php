<?php

namespace App\Http\Controllers;

use App\Group;
use App\Module;
use App\User;
use App\UserProfile;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * 3009255 Natalia
     */
    public function index(Request $request)
    {
        $users_id = \Auth::id();
        $url = $request->path();
        $module = new Module;
        $url = $request->path();
        $user_access = $module->accesos($users_id, $url);

        $users = User::all();
        $module_principals = $module->get_modules_principal_user($users_id);
        $module_menus = $module->get_modules_menu_user($users_id);
        return view('users.index', compact('users', 'user_access', 'module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $module = new Module;
        $users_id = \Auth::id();

        $url=$request->path();
        $user_access = $module->accesos($users_id, $url);

        $groups = Group::pluck('description', 'id');
        $module_principals = $module->get_modules_principal_user($users_id);
        $module_menus = $module->get_modules_menu_user($users_id);

        return view('users.add', compact('user_access','groups', 'module_principals', 'module_menus'));
    }

    public function profiles(Request $request)
    {
        $module = new Module;
        $user = new User;
        $users_id = \Auth::id();

        $url=$request->path();
        $user_access = $module->accesos($users_id, $url);

        $groups = Group::pluck('description', 'id');
        $module_principals = $module->get_modules_principal_user($users_id);
        $module_menus = $module->get_modules_menu_user($users_id);
        $users = $user->get_users_profiles($users_id);

        return view('users.profile', compact('users','user_access','groups', 'module_principals', 'module_menus'));
    }

    public function settings(Request $request)
    {
        $module = new Module;
        $user = new User;
        $users_id = \Auth::id();

        $url=$request->path();
        $user_access = $module->accesos($users_id, $url);

        $groups = Group::pluck('description', 'id');
        $module_principals = $module->get_modules_principal_user($users_id);
        $module_menus = $module->get_modules_menu_user($users_id);
        $users = $user->get_users_profiles($users_id);

        return view('users.settings', compact('users','user_access','groups', 'module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return bool|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changes(Request $request)
    {
        $this->validate($request, [
            'password1' => 'required|min:3',
            'password2'=>'required|min:3',
            'password'=>'required|min:3'
        ]);

        try {

            $iduser = \Auth::id();
            $module = new Module;
            $user = new User;
            $users_id = \Auth::id();

            $url=$request->path();
            $user_access = $module->accesos($users_id, $url);

            $groups = Group::pluck('description', 'id');
            $module_principals = $module->get_modules_principal_user($users_id);
            $module_menus = $module->get_modules_menu_user($users_id);
            $users = $user->get_users_profiles($users_id);


            $users_password = User::where('email',$request->emailp)
                ->where('status','1')
                ->get();



            if($users_password->count()>0){

                $notifys['valor']=2;
                $user = User::find($iduser);
                $user->password = bcrypt($request->password2);
                $user->save();

                return view('users.settings', compact('notifys','users','user_access','groups', 'module_principals', 'module_menus'));

            }else{
                $notifys['valor']=1;
                return view('users.settings', compact('notifys','users','user_access','groups', 'module_principals', 'module_menus'));
            }

        } catch (Exception $e) {
            report($e);
            return false;
        }

    }


    public function update_profile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'email'=>'required|email|min:3',
            'phone'=>'required|min:3',
            'address'=>'required|min:3',
            'city'=>'required|min:3',
            'passport'=>'required|min:3'
        ]);

        try {

            $iduser = \Auth::id();
            $module = new Module;
            $user = new User;
            $users_id = \Auth::id();

            $url=$request->path();
            $user_access = $module->accesos($users_id, $url);

            $groups = Group::pluck('description', 'id');
            $module_principals = $module->get_modules_principal_user($users_id);
            $module_menus = $module->get_modules_menu_user($users_id);
            $users = $user->get_users_profiles($users_id);


            $users_password = User::where('id',$iduser)
                ->where('status','1')
                ->get();



            if($users_password->count()>0)
            {
                $notifys['valor']=3;

                $user = User::find($iduser);
                $user->email = $request->email;
                $user->name = $request->name;
                $user->save();

                UserProfile::where('users_id',$iduser)->update([
                    'address'=>$request->address,
                    'phone'=>$request->phone,
                    'airport'=>$request->airport,
                    'city'=>$request->city,
                    'passport'=>$request->passport
                ]);

                return view('users.settings', compact('notifys','users','user_access','groups', 'module_principals', 'module_menus'));
            }else{
                $notifys['valor']=4;
                return view('users.settings', compact('notifys','users','user_access','groups', 'module_principals', 'module_menus'));
            }

        } catch (Exception $e) {
            report($e);
            return false;
        }

    }


}
