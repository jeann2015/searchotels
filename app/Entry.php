<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    protected $table="entries";

    protected $fillable = [
        'users_id','modules_id','views','inserts','modifys','deletes','status'
    ];
}
