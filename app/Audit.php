<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $table="audits";

    protected $fillable = [
        'users_id','description'
    ];

    /**
     * @param $descriptions
     */
    public function save_audits($descriptions)
    {
        $iduser = \Auth::id();
        $audits = new Audits;
        $audits->users_id = $iduser;
        $audits->description = $descriptions;
        $audits->save();
    }

}
