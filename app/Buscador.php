<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buscador extends Model
{
    private $url = "http://hotel.local/json/";
//    private $url = "http://appbackend.online:8081/json/";



    /**
     * @param string $name
     * @return \Illuminate\Support\Collection|string
     */
    public function get_countries($name="0")
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url."countries-0.json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET"
        ));

        $response = curl_exec($curl);
        $responses = json_decode($response, true);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            if ($name=="0") {
                return collect($responses);
            } else {
                foreach ($responses as $response) {
                    if ($response['name']==$name) {
                        return $response['code'];
                    }
                }
            }
        }
    }

    /**
     * @param $code
     * @return string
     */
    public function get_hotels($code)
    {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 300);

        for ($con=0;$con<=23;$con++) {
            $curl = curl_init();
            $uri = $this->url."hotels-" . $con . ".json";

            curl_setopt_array($curl, array(
                CURLOPT_URL => $uri,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET"
            ));

            $response = curl_exec($curl);
            $responses = collect(json_decode($response, true));

            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return "cURL Error #:" . $err;
            } else {
                foreach ($responses as $responses) {
                    if ($responses['code']==$code) {
                        return $responses;
                    }
                }
            }
        }
    }

    public function get_hotels_search_code($code,$search_code)
    {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 300);

        for ($con=0;$con<=23;$con++) {
            $curl = curl_init();
            $uri = $this->url."hotels-" . $con . ".json";

            curl_setopt_array($curl, array(
                CURLOPT_URL => $uri,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET"
            ));

            $response = curl_exec($curl);
            $responses = collect(json_decode($response, true));

            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return "cURL Error #:" . $err;
            } else {
                foreach ($responses as $responses) {
                    if ($responses['code']==$code) {
                        return $responses;
                    }
                }
            }
        }
    }

    /**
     * @param $code_country
     * @return string
     */
    public function get_destination_code($code_country)
    {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 300);

        $destination_codes=array();

        for ($con=0;$con<=80;$con++) {
            $curl = curl_init();
            $uri = $this->url."destinations-" . $con . ".json";

            curl_setopt_array($curl, array(
                CURLOPT_URL => $uri,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET"
            ));

            $response = curl_exec($curl);
            $responses = collect(json_decode($response, true));

            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return "cURL Error #:" . $err;
            } else {
                foreach ($responses as $response) {
                    if ($response['country']==$code_country) {
                        $destination_codes[] = $response['code'];
                    }
                }
            }
        }


        $code_final_destination = "destination_code=";
        $total_list = count($destination_codes);
        $con=1;

        foreach ($destination_codes as $destination_code) {
            if ($con<$total_list) {
                $code_final_destination .= $destination_code.",";
            } else {
                $code_final_destination .= $destination_code;
            }
            $con=$con+1;
        }

        return $code_final_destination;
    }


    /**
     * @param $value
     * @return string
     */
    public function fecha($value)
    {
        if (! empty($value)) {
            return substr($value, 6, 4) ."-". substr($value, 0, 2) ."-". substr($value, 3, 2);
        }
    }


    /**
     * @param $code_country
     * @return string
     */
    public function get_hotels_code_country($code_country)
    {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 300);
        $hotel_codes=array();

        $cont=1;

        for ($con=0;$con<=23;$con++) {
            $curl = curl_init();
            $uri = $this->url."hotels-" . $con . ".json";

            curl_setopt_array($curl, array(
                CURLOPT_URL => $uri,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET"
            ));

            $response = curl_exec($curl);
            $responses = collect(json_decode($response, true));

            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return "cURL Error #:" . $err;
            } else {
                foreach ($responses as $respon) {
                    if ($respon['country'] == $code_country) {
                        $hotel_codes[] = $respon['code'];
                    }
                }
            }
        }

        $hotel_code_destination="hotel_code=";
        $total_list = count($hotel_codes);
        $con=1;
        foreach ($hotel_codes as $hotel_code) {
            if ($con<$total_list) {
                $hotel_code_destination .= $hotel_code.",";
            } else {
                $hotel_code_destination .= $hotel_code;
            }
            $con=$con+1;
        }

        return $hotel_code_destination;
    }

    /**
     * @param $destination
     * @param $checkin
     * @param $checkout
     * @param $guest
     * @return mixed
     */
    public function get_hotels_sales($destination, $checkin, $checkout, $guest)
    {
        $code_country = $this->get_countries($destination);
        $code_countries = $this->get_hotels_code_country($code_country);

        $checkin_search = $this->fecha($checkin);
        $checkout_search = $this->fecha($checkout);
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api-test.hotelspro.com/api/v2/search/?pax=".$guest."&checkout=".$checkout_search."&checkin=".$checkin_search."&client_nationality=".$code_country."&currency=USD&max_product=10",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $code_countries,

        CURLOPT_HTTPHEADER => array(
            "authorization: Basic Sm91cm5leVRyYXZlbDpzWmQzZmhhcDI1WmR0WlNW",
            "content-type: application/x-www-form-urlencoded"
        )

        ));

        $response = curl_exec($curl);
        $responses = json_decode($response, true);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $responses_debug = $this->debug_sales_hotels($responses);
            return  $responses_debug;
        }
    }

    /**
     * @param $names
     * @return mixed
     */
    public function debug_names($names)
    {
        $count_names = strlen($names);
        if ($count_names>35) {
            $name = explode(" ", $names);
            if (strlen($name[0])>3) {
                return $name[0];
            } else {
                return $name[0]." ".$name[1];
            }
        } else {
            return $names;
        }
    }
    /**
     * @param $results
     * @return array
     */
    public function debug_sales_hotels($results)
    {
//        dd($results);

        $con=1;
        if (isset($results['results'])) {
            foreach ($results['results'] as $result) {
                if ($con <= 12) {
                    $hotel_code = $result['hotel_code'];
                    $price = $result['products'][0]['price'];
                    $description_hotels = $this->get_hotels($hotel_code);
                    $names = $this->debug_names($description_hotels['name']);
                    $responses[$hotel_code]['hotel_code'] = $hotel_code;
                    $responses[$hotel_code]['name'] = $names;
                    $responses[$hotel_code]['long_name'] = $description_hotels['name'];
                    $responses[$hotel_code]['price'] = $price;
                    $responses[$hotel_code]['currencycode'] = $description_hotels['currencycode'];
                    $responses[$hotel_code]['starts'] = $description_hotels['stars'];
                    $responses[$hotel_code]['address'] = $description_hotels['address'];
                    $responses[$hotel_code]['phone'] = $description_hotels['phone'];
                    $responses[$hotel_code]['search_code'] = $results['code'];
                    foreach ($description_hotels['images'] as $image) {
                        $responses[$hotel_code]['images'] = $image['original'];
                    }
                    $con = $con + 1;
                } else {
                    return $responses;
                }
            }
        } else {
            return array();
        }
    }
}
