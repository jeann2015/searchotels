<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id')->unsigned();
            $table->integer('modules_id')->unsigned();
            $table->tinyInteger('views');
            $table->tinyInteger('inserts');
            $table->tinyInteger('modifys');
            $table->tinyInteger('deletes');
            $table->tinyInteger('status');
            $table->timestamps();

            $table->foreign('users_id')->references('id')->on('users');
            $table->foreign('modules_id')->references('id')->on('modules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
