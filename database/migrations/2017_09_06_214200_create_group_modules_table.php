<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('groups_id')->unsigned();
            $table->integer('modules_id')->unsigned();
            $table->tinyInteger('views');
            $table->tinyInteger('inserts');
            $table->tinyInteger('modifys');
            $table->tinyInteger('deletes');
            $table->timestamps();

            $table->foreign('groups_id')->references('id')->on('groups');
            $table->foreign('modules_id')->references('id')->on('modules');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_modules');
    }
}
