<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class EntriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($con = 1; $con <= 4; $con++) {
            DB::table('entries')->insert([
                'users_id' => '1',
                'modules_id' => $con,
                'views' =>'1',
                'inserts' =>'1',
                'modifys' =>'1',
                'deletes' =>'1',
                'status' =>'1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
