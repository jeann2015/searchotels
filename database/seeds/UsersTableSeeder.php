<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrator',
            'email' => 'admin@demo.com',
            'password' => bcrypt('admin'),
            'email_token' => base64_encode('admin@demo.com'),
            'status'=>'1',
            'verified'=>1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('user_profiles')->insert([
            'users_id' => 1,
            'address' => 'Panama',
            'phone' => '63786669',
            'airport' => 'Panama',
            'city'=>'Panama',
            'passport'=>'26262625',
            'images'=>'-',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
