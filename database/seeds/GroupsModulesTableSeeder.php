<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GroupsModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($con = 1; $con <= 4; $con++) {
            DB::table('group_modules')->insert([
                'modules_id' => $con,
                'groups_id' => '1',
                'views' => '1',
                'inserts' => '1',
                'modifys' => '1',
                'deletes' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            if ($con==1) {
                DB::table('group_modules')->insert([
                    'modules_id' => $con,
                    'groups_id' => '2',
                    'views' => '1',
                    'inserts' => '1',
                    'modifys' => '1',
                    'deletes' => '1',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }
        }
    }
}
