<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert([
            'description' => 'Administrators',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('groups')->insert([
            'description' => 'Clients',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
