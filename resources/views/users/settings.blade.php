@extends('layouts.app')

@section('content')

    <!-- Wrap -->
    <div id="wrap">
        <!-- Header -->

        <!--Banner-->
        <section class="sub-banner">
            <!--Background-->
            <div class="bg-parallax bg-1"></div>
            <!--End Background-->
            <!-- Logo -->
            <div class="logo-banner text-center">
                <a href="" title="">
                    {{--<img src="images/logo-banner.png" alt="">--}}
                </a>
            </div>
            <!-- Logo -->
        </section>
        <!--End Banner-->

        <!-- Main -->
        <div class="main">
            <div class="container">
                <div class="main-cn element-page bg-white clearfix">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                     @endif
                        @if(isset($notifys))

                                @foreach ($notifys as $noti => $valor)
                                    @if($valor == 1)
                                        <div class="alert alert-danger" role="alert">
                                            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <strong>El Password No coincide!</strong> No es el Password Actual!
                                        </div>
                                    @elseif($valor == 2)
                                        <div class="alert alert-success" role="alert">
                                            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <strong>El Password Coincide!</strong> Password Cambiado!
                                        </div>

                                    @elseif($valor == 3)
                                        <div class="alert alert-success" role="alert">
                                            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <strong>Listo!</strong> Datos Actualizados!
                                        </div>

                                    @elseif($valor == 4)
                                            <div class="alert alert-danger" role="alert">
                                                <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <strong>No se hizo actualizacion!</strong> Datos No Actualizados!
                                            </div>
                                    @endif
                                @endforeach


                    @endif
                    <!--Breakcrumb-->
                    <section class="breakcrumb-sc">
                        <ul class="breadcrumb arrow">
                            <li><a href="/home"><i class="fa fa-home"></i></a></li>
                            <li>Ajustes</li>
                        </ul>
                    </section>
                    <!--End Breakcrumb-->

                    <section class="user-profile">
                        <div class="main-cn element-page bg-white ">
                            <div class="row">
                                <div class="col-md">

                                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}

                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif


                                    </form>
                                </div>

                            </div>
                        </div>
                    </section>

                    <section class="user-profile">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="user-profile__header">
                                    <h4>Sarah Brown</h4>
                                    <span>Member Since May 2012</span>
                                    <p>
                                        <img src="images/user/img-1.jpg" alt="">
                                    </p>
                                </div>
                                <ul class="user-profile__navigation">
                                    <li><a href="#"><img src="images/icon/icon-cart.png" alt="">Mis Reservaciones</a></li>
                                    <li ><a href="{{route('profile')}}"><img src="images/icon/icon-user.png" alt="">Mi Perfil</a></li>
                                    <li><a href="#"><img src="images/icon/icon-review.png" alt="">Mis comentarios</a></li>
                                    <li class="current"><a href="{{route('settings')}}"><img src="images/icon/icon-setting.png" alt="">Ajustes</a></li>
                                        <li>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                                {{ csrf_field() }}
                                                <a onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();"><img src="images/icon/icon-back.png" alt="">Salir</a>
                                            </form>
                                        </li>

                                </ul>
                            </div>
                            <div class="col-md-9">
                                <h2 class="user-profile__title">Mi Perfil</h2>

                                <div class="my-profile">
                                    <div class="user-form">
                                    < class="row">
                                        {!! Form::open(array('url' => 'update','name'=>'form2')) !!}
                                        <div class="col-md-6">
                                            <h4 class="my-profile__title">Informacion Personal</h4>
                                            <ul>
                                                <li><span>Nombre</span>
                                                    <div class="field-input">
                                                    {!! Form::text('name', $users->pluck('name')[0] ,array('class' => 'input-text','id'=>'name','required')) !!}
                                                    </div>
                                                </li>
                                                <li><span>Correo</span>
                                                    <div class="field-input">
                                                        {!! Form::text('email', $users->pluck('email')[0] ,array('class' => 'input-text','id'=>'email','required')) !!}
                                                    </div>
                                                </li>
                                                <li><span>Telefono</span>
                                                    <div class="field-input">
                                                    {!! Form::text('phone', $users->pluck('phone')[0] ,array('class' => 'input-text','id'=>'phone','required')) !!}
                                                    </div>
                                                </li>
                                                <h3>Direccion</h3>
                                                <div class="field-input">
                                                    {!! Form::text('airport', $users->pluck('airport')[0] ,array('class' => 'input-text','id'=>'airport','required')) !!}
                                                </div>
                                                <div class="field-input">
                                                    {!! Form::text('address', $users->pluck('address')[0] ,array('class' => 'input-text','id'=>'address','required')) !!}
                                                </div>
                                                <div class="field-input">
                                                    {!! Form::text('city', $users->pluck('city')[0] ,array('class' => 'input-text','id'=>'city','required')) !!}
                                                </div>
                                                <div class="field-input">
                                                    {!! Form::text('passport', $users->pluck('passport')[0] ,array('class' => 'input-text','id'=>'passport','required')) !!}
                                                </div>
                                                <div class="field-input">
                                                    <button class="awe-btn awe-btn-1 awe-btn-medium">Guardar Cambios</button>
                                                </div>

                                            </ul>
                                        </div>
                                        {!! Form::close() !!}


                                        <div class="col-md-6">
                                            {!! Form::open(array('url' => 'changes')) !!}
                                                <h3>Cambio de Password</h3>
                                                <div class="field-input">
                                                    <input name="password1" required type="text" class="input-text" placeholder="Password Actual" value="">
                                                    {!! Form::hidden('emailp', $users->pluck('email')[0] ,array('class' => 'input-text','id'=>'emailp')) !!}
                                                </div>
                                                <div class="field-input">
                                                    <input name="password" required type="password" placeholder="Nuevo Password" class="input-text" value="">
                                                </div>
                                                <div class="field-input">
                                                    <input name="password2" required type="password" placeholder="Nuevo Password Otra vez" class="input-text" value="">
                                                </div>
                                                <div class="field-input">
                                                    <button class="awe-btn awe-btn-1 awe-btn-medium">Guardar Contraseña</button>
                                                </div>
                                            {!! Form::close() !!}
                                        </div>
                                        </div>



                                    </div>



                                    {{--<p>--}}
                                        {{--Sign up now for our regular promotional emails packed with special offers and exclusive deals. You can personalise the offers you receive from us by selecting your favorite cities--}}
                                    {{--</p>--}}

                                    {{--<div class="check-box">--}}
                                        {{--<input type="checkbox" id="my-profile__checkbox">--}}
                                        {{--<label for="my-profile__checkbox">Yes, I would like to receive Special Offers &amp; Promotions from Agoda</label>--}}
                                    {{--</div>--}}
                                    {{--<a href="#" class="btn">Editar Perfil</a>/--}}
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!-- End Main -->


    </div>
@endsection
