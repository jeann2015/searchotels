@extends('layouts.app')

@section('content')

    <!-- Wrap -->
    <div id="wrap">
        <!-- Header -->

        <!--Banner-->
        <section class="sub-banner">
            <!--Background-->
            <div class="bg-parallax bg-1"></div>
            <!--End Background-->
            <!-- Logo -->
            <div class="logo-banner text-center">
                <a href="" title="">
                    {{--<img src="images/logo-banner.png" alt="">--}}
                </a>
            </div>
            <!-- Logo -->
        </section>
        <!--End Banner-->

        <!-- Main -->
        <div class="main">
            <div class="container">
                <div class="main-cn element-page bg-white clearfix">
                    <!--Breakcrumb-->
                    <section class="breakcrumb-sc">
                        <ul class="breadcrumb arrow">
                            <li><a href="/home"><i class="fa fa-home"></i></a></li>
                            <li>Users</li>
                        </ul>
                    </section>
                    <!--End Breakcrumb-->

                    <section class="user-profile">
                        <div class="main-cn element-page bg-white ">
                            <div class="row">
                                <div class="col-md">
                                    <h2 class="user-profile__title">Users</h2>
                                    <a href="users/add/" class="awe-btn awe-btn-1 awe-btn-small">Add New User</a>
                                    <p>Users Aplication</p>
                                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}

                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                        <table id="General" class="table">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Nombre</th>
                                                <th>Correo</th>
                                                <th>Acciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($users as $user)
                                                <tr>
                                                    <td>{{ $user->id }}</td>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>
                                                        <div class="tabledit-toolbar btn-toolbar">
                                                            <div class="btn-group btn-group-sm">
                                                                @foreach ($user_access as $user_acces)
                                                                    @if($user_acces->modifys == 1)
                                                                        <a href="users/edit/{{ $user->id }}" class="awe-btn awe-btn-1 awe-btn-small">Edit</a>
                                                                    @else
                                                                        <a href="#" class="awe-btn awe-btn-1 awe-btn-small">Edit</a>
                                                                    @endif
                                                                    @if($user_acces->deletes==1)
                                                                        <a href="users/delete/{{ $user->id }}" class="awe-btn awe-btn-1 awe-btn-small">Delete</a>
                                                                    @else
                                                                        <a href="#" class="btn btn-default" role="button">No Delete</a>
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!-- End Main -->


    </div>
@endsection
