@extends('layouts.app')

@section('content')

    <!-- Wrap -->
    <div id="wrap">
        <!-- Header -->

        <!--Banner-->
        <section class="sub-banner">
            <!--Background-->
            <div class="bg-parallax bg-1"></div>
            <!--End Background-->
            <!-- Logo -->
            <div class="logo-banner text-center">
                <a href="" title="">
                    {{--<img src="images/logo-banner.png" alt="">--}}
                </a>
            </div>
            <!-- Logo -->
        </section>
        <!--End Banner-->

        <!-- Main -->
        <div class="main">
            <div class="container">
                <div class="main-cn element-page bg-white clearfix">
                    <!--Breakcrumb-->
                    <section class="breakcrumb-sc">
                        <ul class="breadcrumb arrow">
                            <li><a href="/home"><i class="fa fa-home"></i></a></li>
                            <li>Ajustes</li>
                        </ul>
                    </section>
                    <!--End Breakcrumb-->

                    <section class="user-profile">
                        <div class="main-cn element-page bg-white ">
                            <div class="row">
                                <div class="col-md">

                                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}

                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif


                                    </form>
                                </div>

                            </div>
                        </div>
                    </section>

                    <section class="user-profile">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="user-profile__header">
                                    <h4>Sarah Brown</h4>
                                    <span>Member Since May 2012</span>
                                    <p>
                                        <img src="images/user/img-1.jpg" alt="">
                                    </p>
                                </div>
                                <ul class="user-profile__navigation">
                                    <li><a href="#"><img src="images/icon/icon-cart.png" alt="">Mis Reservaciones</a></li>
                                    <li class="current"><a href="{{route('profile')}}"><img src="images/icon/icon-user.png" alt="">Mi Perfil</a></li>
                                    <li><a href="#"><img src="images/icon/icon-review.png" alt="">Mis comentarios</a></li>
                                    <li><a href="{{route('settings')}}"><img src="images/icon/icon-setting.png" alt="">Ajustes</a></li>

                                    <li>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                            {{ csrf_field() }}
                                            <a onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();"><img src="images/icon/icon-back.png" alt="">Salir</a>
                                        </form>
                                    </li>


                                </ul>
                            </div>
                            <div class="col-md-9">
                                <h2 class="user-profile__title">Mi Perfil</h2>

                                <div class="my-profile">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4 class="my-profile__title">Personal Information</h4>
                                            <ul>
                                                <li><span>Nombre</span>{{$users->pluck('name')[0]}}</li>
                                                <li><span>Correo</span>{{$users->pluck('email')[0]}}</li>
                                                <li><span>Telefono</span>{{$users->pluck('phone')[0]}}</li>

                                            </ul>
                                        </div>

                                        <div class="col-md-6">
                                            <h4 class="my-profile__title">Direccion</h4>
                                            <ul>
                                                <li><span>Aeropuerto</span>{{$users->pluck('airport')[0]}}</li>
                                                <li><span>Direccion</span>{{$users->pluck('address')[0]}}</li>
                                                <li><span>Ciudad</span>{{$users->pluck('city')[0]}}</li>
                                                <li><span>Pasaporte</span>{{$users->pluck('passport')[0]}}</li>
                                            </ul>
                                        </div>
                                    </div>

                                    {{--<p>--}}
                                        {{--Sign up now for our regular promotional emails packed with special offers and exclusive deals. You can personalise the offers you receive from us by selecting your favorite cities--}}
                                    {{--</p>--}}

                                    {{--<div class="check-box">--}}
                                        {{--<input type="checkbox" id="my-profile__checkbox">--}}
                                        {{--<label for="my-profile__checkbox">Yes, I would like to receive Special Offers &amp; Promotions from Agoda</label>--}}
                                    {{--</div>--}}
                                    <a href="{{route('settings')}}" class="btn">Editar Perfil</a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!-- End Main -->


    </div>
@endsection
