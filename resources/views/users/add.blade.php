@extends('layouts.app')

@section('content')

    <div id="wrap">
        <!-- Header -->

        <!--Banner-->
        <section class="sub-banner">
            <!--Background-->
            <div class="bg-parallax bg-1"></div>
            <!--End Background-->
            <!-- Logo -->
            <div class="logo-banner text-center">
                <a href="" title="">
                    {{--<img src="images/logo-banner.png" alt="">--}}
                </a>
            </div>
            <!-- Logo -->
        </section>
        <!--End Banner-->

        <!-- Main -->
        <div class="main">
            <div class="container">
                <div class="main-cn element-page bg-white clearfix">
                    <!--Breakcrumb-->
                    <section class="breakcrumb-sc">
                        <ul class="breadcrumb arrow">
                            <li><a href="/users"><i class="fa fa-home"></i></a></li>
                            <li>Add New User</li>
                        </ul>
                        {{--<div class="support float-right">--}}
                        {{--<small>Got a question?</small> 123-123-1234--}}
                        {{--</div>--}}
                    </section>
                    <!--End Breakcrumb-->

                    <section class="user-profile">
                        <div class="user-form user-signup">


                            <div class="row">
                                <div class="col-md">
                                    <h2 class="user-profile__title">New User</h2>
                                    <form class="form-horizontal" method="POST" action="{{ route('users/new') }}">
                                        {{ csrf_field() }}

                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                        <div class="form-search clearfix">
                                            Name:
                                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                        <div class="field-input">
                                            Email:
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                            @endif

                                        </div>
                                        <div class="field-input">
                                            Password:
                                            <input id="email" type="password" class="form-control" name="password" value="{{ old('password') }}" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                            @endif

                                        </div>
                                        <div class="field-input">
                                            Confirm Password:
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                        </div>
                                        Group User:
                                        <br>
                                        <div class="form-hotel field-select">
                                            {{--<span data-placeholder="Select">Administrator</span>--}}
                                            {!! Form::select('groups_id', $groups, '', ['class'=>'form-control','required']); !!}
                                        </div>
                                        <br>
                                        <div class="field-input">

                                            <button type="submit" class="awe-btn awe-btn-1 awe-btn-small">
                                                Save
                                            </button>

                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!-- End Main -->


    </div>
@endsection
