@extends('layouts.app')

@section('content')

<!-- Wrap -->
<div id="wrap">
    <!-- Header -->

    <!--Banner-->
    <section class="sub-banner">
        <!--Background-->
        <div class="bg-parallax bg-1"></div>
        <!--End Background-->
        <!-- Logo -->
        <div class="logo-banner text-center">
            <a href="" title="">
                {{--<img src="images/logo-banner.png" alt="">--}}
            </a>
        </div>
        <!-- Logo -->
    </section>
    <!--End Banner-->

    <!-- Main -->
    <div class="main">
        <div class="container">
            <div class="main-cn element-page bg-white clearfix">
                <!--Breakcrumb-->
                <section class="breakcrumb-sc">
                    <ul class="breadcrumb arrow">
                        <li><a href="/"><i class="fa fa-home"></i></a></li>
                        <li>Login</li>
                    </ul>
                    {{--<div class="support float-right">--}}
                        {{--<small>Got a question?</small> 123-123-1234--}}
                    {{--</div>--}}
                </section>
                <!--End Breakcrumb-->

                <section class="user-profile">
                    <div class="user-form user-signup">


                        <div class="row">
                            <div class="col-md">
                                <h2 class="user-profile__title">Sign in</h2>
                                <p>Access your account information and manage your bookings.</p>
                                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}

                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="field-input">
                                    <input type="email" required class="input-text" name="email" value="Email *">

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="field-input">
                                    <input type="password" required class="input-text" name="password" value="">
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif

                                </div>
                                <div class="field-input">
                                    <div class="check-box">
                                        <input type="checkbox" id="checkbox">
                                        <label for="checkbox">Keep me logged in</label>
                                    </div>
                                    <a href="#">Get a new password</a>
                                </div>
                                <div class="field-input">
                                    <button class="awe-btn awe-btn-1 awe-btn-medium">Sign in</button>
                                </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- End Main -->


</div>
@endsection
