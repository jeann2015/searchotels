<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:300,400,600' rel='stylesheet' type='text/css'>

    <link href="{{ asset('book/css/library/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('book/css/library/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('book/css/library/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('book/css/library/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('book/css/library/jquery.mb.YTPlayer.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('book/css/library/magnific-popup.css') }}">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css">

    <link rel="stylesheet" href="{{ asset('book/css/style.css') }}">

</head>
<body>
    <div id="app">
        <!--Navigation-->
        <nav class="navigation nav-c" id="navigation" data-menu-type="1200">
            <div class="nav-inner">
                <a href="#" class="bars-close" id="bars-close">Close</a>
                <div class="tb">
                    <div class="tb-cell">
                        <ul class="menu-list text-uppercase">
                            @guest
                            <li ><a href="{{ url('/') }}">{{ config('app.name', 'Laravel') }}</a></li>
                            <li ><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                    {{ csrf_field() }}
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                    </li>
                                </form>
                            @else
                                <li ><a href="{{ url('/') }}">{{ config('app.name', 'Laravel') }}</a></li>
                                @if(isset($module_principals))
                                    @foreach($module_principals as $module_principal)
                                        <li >
                                            <a role="button" aria-expanded="false" href="{{ url('/') }}">{{ $module_principal->description }}
                                            </a>

                                            <ul class="sub-menu" role="menu">
                                                @foreach ($module_menus as $module_menu)
                                                    <li>
                                                        <a href="{{ route( $module_menu->url ) }}">
                                                            {{ $module_menu->description }}
                                                        </a>

                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                            Logout
                                                        </form>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                @endif

                            <li class="current-menu-parent">
                                <a href="#" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }}
                                    {{--<span class="caret"></span>--}}
                                </a>

                                <ul class="sub-menu" role="menu">

                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!--End Navigation-->
        <!-- Preloader -->
        <div id="preloader">
            <div class="tb-cell">
                <div id="page-loading">
                    <div></div>
                    <p>Loading</p>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Branding Image -->

                </div>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <ul class="sub-menu">
                        &nbsp;
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script type="text/javascript" src="{{ asset('book/js/library/jquery-1.11.0.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.2/axios.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="{{ asset('book/js/library/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('book/js/library/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('book/js/library/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('book/js/library/parallax.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('book/js/library/jquery.nicescroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('book/js/library/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('book/js/library/jquery.mb.YTPlayer.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('book/js/library/SmoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('book/js/library/jquery.magnific-popup.js') }}"></script>



    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>


    <script type="text/javascript" src="{{ asset('book/js/script.js') }}"></script>


    <script>
        $(document).ready(function() {

            $('#General').DataTable({
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                }
            });

        });
    </script>

</body>
</html>
