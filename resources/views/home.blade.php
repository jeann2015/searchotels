@extends('layouts.app')

@section('content')


    <!--Banner-->
    <section class="banner">

        <!-- Banner Video Background -->
        <div class="banner-video" id="banner-video">\
            <div class="bg-fixed bg-1"></div>
            <div class="banner-video-control">
                <span class="icon-play fa fa-play"></span>
            </div>
            <a id="banner-player" data-property="{videoURL:'https://www.youtube.com/watch?v=wJF5NXygL4k'}">Video Background</a>
        </div>
        <!-- Banner Video Background -->

        <div class="container">

            <div class="logo-banner text-center">
                <a href="" title="">
                    <img src="images/logo-banner.png" alt="">
                </a>
            </div>

            <!-- Banner Content -->
            <div class="banner-cn">

                <!-- Tabs Cat Form -->
                <ul class="tabs-cat text-center row">
                    {{--<li class="cate-item col-xs-2">--}}
                        {{--<a data-toggle="tab" href="#form-flight" title="">--}}
                            {{--<span>flight</span>--}}
                            {{--<img src="images/icon-flight.png" alt="">--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    <li class="cate-item active col-xs-2">
                        <a data-toggle="tab" href="#form-hotel" title=""><span>Hotel</span><img src="images/icon-hotel.png" alt=""></a>
                    </li>
                    {{--<li class="cate-item col-xs-2">--}}
                        {{--<a data-toggle="tab" href="#form-car" title=""><span>Car</span><img src="images/icon-car.png" alt=""></a>--}}
                    {{--</li>--}}
                    {{--<li class="cate-item col-xs-2">--}}
                        {{--<a data-toggle="tab" href="#form-package" title=""><span>package deals</span><img src="images/icon-tour.png" alt=""></a>--}}
                    {{--</li>--}}
                    {{--<li class="cate-item col-xs-2">--}}
                        {{--<a data-toggle="tab" href="#form-cruise" title=""><span>cruise</span><img src="images/icon-cruise.png" alt=""></a>--}}
                    {{--</li>--}}
                    {{--<li class="cate-item col-xs-2">--}}
                        {{--<a data-toggle="tab" href="#form-tour" title=""><span>TOUR</span><img src="images/icon-vacation.png" alt=""></a>--}}
                    {{--</li>--}}
                </ul>
                <!-- End Tabs Cat -->

                <!-- Tabs Content -->
                <div class="tab-content">

                    <!-- Search Hotel -->
                    {!! Form::open(array('url' => 'search/searching')) !!}
                    <div class="form-cn form-hotel tab-pane active in" id="form-hotel">
                        <h2>Where would you like to go?</h2>
                        <div class="form-search clearfix">
                            <div class="form-field field-destination">

                                @if(isset($datos['destination']))
                                    <input list="countries" value="{{$datos['destination']}}" type="text" required id="destination" name="destination" class="field-input">
                                @else
                                    <input list="countries" type="text" required id="destination" name="destination" class="field-input" placeholder="Destination">
                                @endif
                                <datalist id="countries">
                                    @foreach($countries as $countrie)
                                        <option value="{{$countrie['name']}}">{{$countrie['code']}}</option>
                                    @endforeach
                                </datalist>
                            </div>
                            <div class="form-field field-date">
                                @if(isset($datos['checkin']))
                                    <input type="text" value="{{$datos['checkin']}}" name="checkin" required class="field-input calendar-input" placeholder="Check in">
                                @else
                                    <input type="text" name="checkin" required class="field-input calendar-input" placeholder="Check in">
                                @endif
                            </div>
                            <div class="form-field field-date">
                                @if(isset($datos['checkout']))
                                    <input type="text" value="{{$datos['checkout']}}" name="checkout" required class="field-input calendar-input" placeholder="Check out">
                                @else
                                    <input type="text" name="checkout" required class="field-input calendar-input" placeholder="Check out">
                                @endif
                            </div>
                            <div class="form-field field-select">
                                <div class="select">
                                    @if(isset($datos['guest']))
                                        <span>{{$datos['guest']." Guest"}}</span>
                                    @else
                                        <span>Guest</span>
                                    @endif
                                    <select required name="guest">
                                        @if(isset($datos['guest']) && $datos['guest']<> "")
                                            <option @if(isset($datos['guest']) && $datos['guest']==1) {{ 'selected'}} @endif  value="1">1 Guest</option>
                                            <option @if(isset($datos['guest']) && $datos['guest']==2) {{ ' selected '}} @endif value="2">2 Guest</option>
                                            <option @if(isset($datos['guest']) && $datos['guest']==3) {{ ' selected '}} @endif value="3">3 Guest</option>
                                        @else
                                            <option value="1">1 Guest</option>
                                            <option value="2">2 Guest</option>
                                            <option value="3">3 Guest</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-submit">
                                <button type="submit" class="awe-btn awe-btn-lager awe-search">Search</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div class="hotel-grid-cn clearfix">
                            <!-- Hotel Item -->
                            <br>
                            @if(isset($results) && count($results)>=1 && !isset($results['0']))

                                @php
                                    $con=1;
                                @endphp

                                @foreach($results as $result)
                                    @if($con>0 && $con<=3)
                                        <div class="col-xs-6 col-sm-4 col-md-6 col-lg-4">
                                            <div class="hotel-item">
                                                <figure class="hotel-img">
                                                    <a target="_blank" href="{{ url('/search/hotels/'.$result['hotel_code'].'/'.$result['search_code']) }}" title="{{$result['long_name']}}">
                                                        <img width="295" height="196" src="{{$result['images']}}" alt="">
                                                    </a>
                                                    {{--<figcaption>--}}
                                                    {{--Save <span>30</span>%--}}
                                                    {{--</figcaption>--}}
                                                </figure>
                                                <div class="hotel-text">
                                                    <div class="hotel-name">
                                                        <a target="_blank" href="{{ url('/search/hotels/'.$result['hotel_code'].'/'.$result['search_code']) }}" title="">{{$result['name']}}</a>
                                                    </div>
                                                    <div class="hotel-places">
                                                        <a href="" title="">{{$result['address']}}</a>
                                                    </div>
                                                    <div class="hotel-places">
                                                        <a href="" title="">Stars:{{$result['starts']}}</a>
                                                    </div>
                                                    <hr class="hr">
                                                    <div class="price-box">
                                                        <span class="price old-price">From</span>
                                                        <span class="price special-price">{{$result['price']}}<small>/night</small></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if($con>3 && $con<=6)

                                        <div class="col-xs-6 col-sm-4 col-md-6 col-lg-4">

                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>

                                            <div class="hotel-item">
                                                <figure class="hotel-img">
                                                    <a target="_blank" href="{{ url('/search/hotels/'.$result['hotel_code'].'/'.$result['search_code']) }}" title="{{$result['long_name']}}">
                                                        <img width="295" height="196" src="{{$result['images']}}" alt="">
                                                    </a>
                                                    {{--<figcaption>--}}
                                                    {{--Save <span>30</span>%--}}
                                                    {{--</figcaption>--}}
                                                </figure>
                                                <div class="hotel-text">
                                                    <div class="hotel-name">
                                                        <a target="_blank" href="{{ url('/search/hotels/'.$result['hotel_code'].'/'.$result['search_code']) }}" title="">{{$result['name']}}</a>
                                                    </div>
                                                    <div class="hotel-places">
                                                        <a href="" title="">{{$result['address']}}</a>
                                                    </div>
                                                    <div class="hotel-places">
                                                        <a href="" title="">starts:{{$result['starts']}}</a>
                                                    </div>
                                                    <hr class="hr">
                                                    <div class="price-box">
                                                        <span class="price old-price">From</span>
                                                        <span class="price special-price">{{$result['price']}}<small>/night</small></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if($con>6 && $con<=9)

                                        <div class="col-xs-6 col-sm-4 col-md-6 col-lg-4">

                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>

                                            <div class="hotel-item">
                                                <figure class="hotel-img">
                                                    <a target="_blank" href="{{ url('/search/hotels/'.$result['hotel_code'].'/'.$result['search_code']) }}" title="{{$result['long_name']}}">
                                                        <img width="295" height="196" src="{{$result['images']}}" alt="">
                                                    </a>
                                                    {{--<figcaption>--}}
                                                    {{--Save <span>30</span>%--}}
                                                    {{--</figcaption>--}}
                                                </figure>
                                                <div class="hotel-text">
                                                    <div class="hotel-name">
                                                        <a target="_blank" href="{{ url('/search/hotels/'.$result['hotel_code'].'/'.$result['search_code']) }}" title="">{{$result['name']}}</a>
                                                    </div>
                                                    <div class="hotel-places">
                                                        <a href="" title="">{{$result['address']}}</a>
                                                    </div>
                                                    <div class="hotel-places">
                                                        <a href="" title="">Stars:{{$result['starts']}}</a>
                                                    </div>
                                                    <hr class="hr">
                                                    <div class="price-box">
                                                        <span class="price old-price">From</span>
                                                        <span class="price special-price">{{$result['price']}}<small>/night</small></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if($con>9 && $con<=12)

                                        <div class="col-xs-6 col-sm-4 col-md-6 col-lg-4">

                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>

                                            <div class="hotel-item">
                                                <figure class="hotel-img">
                                                    <a target="_blank" href="{{ url('/search/hotels/'.$result['hotel_code'].'/'.$result['search_code']) }}" title="{{$result['long_name']}}">
                                                        <img width="295" height="196" src="{{$result['images']}}" alt="">
                                                    </a>
                                                    {{--<figcaption>--}}
                                                    {{--Save <span>30</span>%--}}
                                                    {{--</figcaption>--}}
                                                </figure>
                                                <div class="hotel-text">
                                                    <div class="hotel-name">
                                                        <a target="_blank" href="{{ url('/search/hotels/'.$result['hotel_code'].'/'.$result['search_code']) }}" title="">{{$result['name']}}</a>
                                                    </div>
                                                    <div class="hotel-places">
                                                        <a href="" title="">{{$result['address']}}</a>
                                                    </div>
                                                    <div class="hotel-places">
                                                        <a href="" title="">Stars:{{$result['starts']}}</a>
                                                    </div>
                                                    <hr class="hr">
                                                    <div class="price-box">
                                                        <span class="price old-price">From</span>
                                                        <span class="price special-price">{{$result['price']}}<small>/night</small></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @php
                                        $con=$con+1;
                                    @endphp
                                @endforeach

                            @else
                                @if(isset($results) && !isset($results['0']))
                                    <div class="col-xs-12">
                                        <div class="hotel-item">
                                            <h1>
                                                No results
                                            </h1>
                                        </div>
                                    </div>
                                @endif
                        @endif
                        <!-- End Hotel Item -->

                            <!-- End Search Car -->

                        </div>
                        <!-- End Tabs Content -->

                        <!-- Search Car -->
                        <br>
                        <br>


                    </div>
                    <!-- End Banner Content -->

                </div>

    </section>

    <!--End Banner-->


@endsection

